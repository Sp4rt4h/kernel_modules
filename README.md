# A repo of kernel modules for learning

Youtube video explanations: https://www.youtube.com/channel/UCwUv_K97UCOIP2wSIoPCm_g

## modules have some sample modules for interacting with the kernel

To test a sample module:
1. Go to module directory
2. Run ./genmake
3. Run make
4. sudo insmod name_of_module.ko
5. sudo dmesg to view output
6. sudo lsmod to view module loaded
7. sudo rmmod to remove the module

## device_drivers have sample drivers for running on the system

To test a device driver:
1. Go to device driver directory
2. Build and insert the device driver
3. use sudo mknod and choose the corresponding major and minor numbers in the
driver
example: mknod -m 666 /dev/mydev c major minor
4. Compile and run the test program, making sure the path is to the special file
that you created in the previous step
