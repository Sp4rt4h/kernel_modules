/*
Copyright (C) 2018 Matthew Leon

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/


#include "mod_usb.h"

#define MY_USB_NAME "Matthew Leon's USB Device Driver"

static int my_usb_probe(struct usb_interface *intf, const struct usb_device_id *id)
{
	pr_info("My usb driver probe just got called. Device ID is: %d\n", id->idProduct);

	pr_info("My usb driver intf->dev.init_name is: %s\n", intf->dev.init_name);

	dump_stack();
	return 0;
}

static void my_usb_disconnect(struct usb_interface *intf)
{

}

//Set the file operations for the device driver
static const struct file_operations my_usb_dev_fops = {
	.owner = THIS_MODULE,
 };

static const struct usb_device_id my_table[] = {
	{ USB_DEVICE(0xDEAD, 0xBEEF) },
	{ USB_DEVICE(0x0781, 0x5597) },
	{ USB_DEVICE(0xFFE0, 0xFFE0) },
	{ }
};

MODULE_DEVICE_TABLE(usb, my_table);

static struct usb_driver my_usb_driver = {
	.name = "USB_DRIVER",
	.probe = my_usb_probe,
	.disconnect = my_usb_disconnect,
	.id_table = my_table,
};


static int __init my_init(void)
{
	int drvr_reg_succ = 0;
	
 	pr_info("Starting the usb driver\n");

	drvr_reg_succ = usb_register(&my_usb_driver);

	return 0;
}

static void __exit my_exit(void)
{
	pr_info("Stopping the usb driver\n");

	usb_deregister(&my_usb_driver);

	pr_info("usb_deregister\n");

}

module_init(my_init);
module_exit(my_exit);

MODULE_AUTHOR("Matthew Leon");
MODULE_LICENSE("GPL v2");		
