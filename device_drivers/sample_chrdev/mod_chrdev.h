/*
Copyright (C) 2018 Matthew Leon

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <linux/init.h>      	
#include <linux/fs.h>       	//File operations    
#include <linux/uaccess.h>	//user/kernel space communications
#include <linux/slab.h>		//memory allocation
#include <linux/cdev.h>		//character device utils
#include <linux/device.h>

static int  __init my_init(void);
static void __exit my_exit(void);

MODULE_AUTHOR("Matthew Leon");
MODULE_LICENSE("GPL v2");
