/*
Copyright (C) 2018 Matthew Leon

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <linux/module.h>
#include "mod_chrdev.h"

#define MY_CHARDEV_NAME "Matthew Leon's Character Device"
#define MEM_SIZE 256

static char *memory = NULL;		//Main buffer for character driver
static char *file_name = NULL;
static char *final_name = NULL;
static dev_t first_dev = { 0 };
static unsigned int minor_count = 2;
static int major = 1307, minor = 0;	//Needed for device drivers
static struct cdev *my_char_dev = NULL;
static int counter = 0;                 //Number of times the device has been opened

static inline int my_dev_open(struct inode *inode, struct file *file)
{
	counter = counter + 1;
	
	pr_info("Opening the device\n");
	pr_info("Major number = %u, Minor number: %u\n",
		imajor(inode), iminor(inode));
	
	pr_info("Number of times this device has been opened: %d\n", counter);
	
	return 0;
} 

static inline int my_dev_close(struct inode *inode, struct file * file)
{
	pr_info( "Closing the device\n");
	return 0;
}

static inline ssize_t my_dev_read(struct file *file, char __user *buf,
				size_t num, loff_t *pos)
{
	ssize_t ret = -1;

	if(num > MEM_SIZE) {
		pr_warn("Attempted buffer overread\n");
		return ret;
	}

	//Get device file path
	final_name = d_path(&file->f_path, file_name, MEM_SIZE);

	pr_info("Reading from device: %s\n", final_name);
	pr_info("Copying: \"%s\" to user-space\n", memory);

	ret = (ssize_t)copy_to_user(buf, memory, num);

	pr_info("We wrote %d bytes to user-space\n", (int)(num - ret));

	//copy_to_user returns number of bytes NOT copied
	return num - ret;
}

static inline ssize_t my_dev_write
	(struct file *file, const char __user *buf, size_t num, loff_t *pos)
{
	ssize_t ret = -1;

	if(num > MEM_SIZE) {
		pr_warn("Attempted buffer overwrite\n");
		return ret;
	}

	final_name = d_path(&file->f_path, file_name, MEM_SIZE);

	pr_info("Writing to device: %s\n", final_name);
	
	ret = (ssize_t)copy_from_user(memory, buf, num);

	pr_info("We wrote %d bytes to kernel-space\n", (int)(num - ret));
	
	return num - ret;
}


//Set the file operations for the device driver
static const struct file_operations my_char_dev_fops = {
	.owner = THIS_MODULE,
	.open  = my_dev_open,
	.release = my_dev_close,
	.read  = my_dev_read,
	.write = my_dev_write,
 };

static int __init my_init(void)
{
        int ret_val = -1;
	pr_info("Allocating, Setting, and Registering char_dev\n");

	memory    = kmalloc(MEM_SIZE, GFP_KERNEL);
	file_name = kmalloc(MEM_SIZE, GFP_KERNEL);

	//Old way to register a character device
	//create dev_t
	first_dev = MKDEV(major, minor);

	//register device numbers
	ret_val = register_chrdev_region(first_dev, minor_count, MY_CHARDEV_NAME);
	if (0 != ret_val) {
		pr_emerg("Failed to register major and minor numbers!!\n");
		return -1;
	}
	//allocate cdev_t
	my_char_dev = cdev_alloc();
	if (NULL == my_char_dev) {
		pr_emerg("Failed to allocate character device!!\n");
		return -1;
	}
	//initialize the device
	cdev_init(my_char_dev, &my_char_dev_fops);

	//make the device work
	ret_val = cdev_add(my_char_dev, first_dev, minor_count);
	if (0 > ret_val) {
		pr_emerg("Failed to run the device	!!\n");
		return -1;
	}

	pr_info("Finished allocating resources and registering device\n");
	pr_info("Major number is: %d, and number of minor numbers is: %d\n", 
		major, minor_count);

	return 0;
}

static void __exit my_exit(void)
{
	pr_info("Deallocating and releasing char_dev\n");

	//release the device
	cdev_del(my_char_dev);

	//unregister the dev_t
	unregister_chrdev_region(first_dev, minor_count);

	kfree(final_name);
	kfree(file_name);
	kfree(memory);
	
	pr_info("Finished deallocating char_dev and releasing resources\n");
}

module_init(my_init);
module_exit(my_exit);

MODULE_AUTHOR("Matthew Leon");
MODULE_LICENSE("GPL v2");		
