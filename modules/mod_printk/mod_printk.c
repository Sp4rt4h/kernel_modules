/*Copyright (C) 2018 Matthew Leon

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include <linux/module.h>	//This header file holds important functions 
				//that we need for loading and unloading 
				//the module into the kernel

static int __init my_init(void)
{
	printk( KERN_INFO "Hello world!!!!\n");
	return 0;
}

static void __exit my_exit(void)
{
	printk( KERN_ALERT "Goodbye world!!\n");
}

module_init(my_init);		//Specify init function
module_exit(my_exit);		//specify exit_function

MODULE_AUTHOR("Matthew Leon");
MODULE_LICENSE("GPL v2");
