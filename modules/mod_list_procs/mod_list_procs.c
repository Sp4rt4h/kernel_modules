/*
Copyright (C) 2018 Matthew Leon

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <linux/module.h>
#include <linux/sched.h>		//Holds important structs and functions

static int __init my_init(void)
{
	struct task_struct *proc = NULL;

	for_each_process(proc) {	//Use this loop to iterate processes
	//for( proc = &init_task; (proc = next_task(proc)) != &init_task;) 
		printk( KERN_INFO "Process %s ID: %d\n", 
			proc->comm, proc->tgid);
	}

	return 0;
}

static void __exit my_exit(void)
{
}

module_init(my_init);
module_exit(my_exit);

MODULE_AUTHOR("Matthew Leon");
MODULE_LICENSE("GPL v2");
