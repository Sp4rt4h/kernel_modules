/*
Copyright (C) 2018 Matthew Leon

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <linux/module.h>
#include <linux/slab.h>		//Needed for memory allocation

int *int_arr = NULL;

static int __init my_init(void)
{
	printk(KERN_INFO "Allocating memory...\n");

	while( 1 == 1) {
		
		//flag options at linux/gfp.h
		int_arr = (int *)kmalloc(4096, GFP_KERNEL);//Allocate array
		if(NULL == int_arr) {
			printk(KERN_EMERG "int_arr returned NULL!\n");
			return -1;
		}
		else {
			printk(KERN_INFO "int_arr value: %p\n", int_arr);
		}
	}



	return 0;
}

static void __exit my_exit(void)
{
	printk(KERN_INFO "Freeing memory...\n");
	kfree(int_arr);					//Free Array
}

module_init(my_init);
module_exit(my_exit);

MODULE_AUTHOR("Matthew Leon");
MODULE_LICENSE("GPL v2");
