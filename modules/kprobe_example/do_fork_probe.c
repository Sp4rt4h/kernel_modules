/*
Copyright (C) 2019 Matthew Leon

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kprobes.h>

static struct kprobe do_fork_probe;
static int count = 0;

static int do_fork_counter(struct kprobe *p, struct pt_regs *regs)
{
	count = count + 1;
	pr_info("_do_fork has been called %d times\n", count);
	return 0;
}

static int __init my_init(void)
{
	int error = -1;

	/*Set up kprobe*/
	do_fork_probe.symbol_name = "_do_fork";
	do_fork_probe.pre_handler = do_fork_counter;

	error = register_kprobe(&do_fork_probe);
	if(-1 == error)
		pr_info("Failed to register probe, error: %d\n", error);

	return 0;
}

static void __exit my_exit(void)
{
	unregister_kprobe(&do_fork_probe);

}

module_init(my_init);
module_exit(my_exit);

MODULE_AUTHOR("Matthew Leon");
MODULE_LICENSE("GPL v2");
